package com.maBanque;

public class CompteImpl implements Compte{ 
 
 	float solde; 
 	 
 	@Override 
 	public void crediter(float credit) throws Exception{ 
 		solde=solde+credit;
 	}

	@Override
	public float getSolde() {
		return solde;
	}

	@Override
	public float debiter(float debit) throws Exception {
		return 0;
	}

	@Override
	public void setSolde(float solde) throws Exception {
		this.solde=solde;
	} 
  
} 
